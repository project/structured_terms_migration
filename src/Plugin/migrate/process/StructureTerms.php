<?php

namespace Drupal\structured_terms_migration\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;

/**
 * @MigrateProcessPlugin(
 *   id = "structure_terms",
 *   handle_multiples = FALSE
 * )
 * 
 * @codingStandardsIgnoreStart
 *
 * To create nested terms from string 'top/middle/lowest':
 * @code
 * field_dragdrop_section:
 *   plugin: structure_terms
 *   source: string
 *   vocabulary: 'vocabulary_machine_name'
 * @endcode
 *
 * @codingStandardsIgnoreEnd
 */
class StructureTerms extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    // Make vocabulary required.
    if (!isset($this->configuration['vocabulary'])) {
      throw new MigrateException('"vocabulary" must be configured.');
    }

    // Skip if no structure data set.
    if (empty($value)) {
      return;
    }

    $previous_tid = 0;
    $vocabulary = $this->configuration['vocabulary'];
    $terms = explode('/', trim($value, '/'));
  
    foreach ($terms as $term_name) {
  
      $term_properties = [
        'name' => $term_name, 
        'vid' => $vocabulary, 
        'parent' => $previous_tid,
      ];
  
      // Lookup term.
      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties($term_properties);
  
      if (empty($terms)) {
        // Create term.
        $term = Term::create($term_properties);
        $term->save();
        $previous_tid = $term->id();
      }
      else {
        // Use existing term.
        $term = reset($terms);
        $previous_tid = $term->id();
      }
  
    }

    // Return tid of deepest nested term.
    return $previous_tid;
  }

}

