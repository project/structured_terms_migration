INTRODUCTION
------------

Migrate a path string into hierarchical terms.



RECOMMENDED MODULES
-------------------

 * Migrate Tools (https://www.drupal.org/project/migrate_tools)

 * Migrate Devel (https://www.drupal.org/project/migrate_devel)

  * Migrate Plus (https://www.drupal.org/project/migrate_plus)


 INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

This module has no configuration.


DEVELOPMENT
-------------

Use in the migrate .yml file that defines your migration.

To create nested terms from a source string 'top/middle/lowest':

field_term_entity_references:
  plugin: structure_terms
  source: source_string
  vocabulary: 'vocabulary_machine_name'
